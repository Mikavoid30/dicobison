
const { GoogleProvider } = require('../../dist/src/scrapper/providers/google.provider.js');

describe('GoogleProvider', function() {
  // When inputs are wrong should throw an error
  describe('getData', () => {
    const service = {
      name: 'google',
      baseUrl: 'https://www.google.co.in',
      paramsTemplate: 'search?hl={{lang}}&q=define+{{text}}',
      langs: ['en', 'fr', 'de']
    }

    it('When languages does not exist it should throw', () => {
      expect(() => new GoogleProvider(service, 'frss', 'ziguergea')).toThrow('language not supported')
    })

    it('Should give back a bison form MAISON in French', (done) => {
      const google = new GoogleProvider(service, 'fr', 'maison', { fakeScraping: true })
      google.getData().then((res) => {
        expect(res).toMatchObject({
          definitions: ['Bâtiment d\'habitation (immeuble, logement, résidence) ; spécialement bâtiment conçu pour un seul ou un petit nombre de foyers (pavillon, villa).',
        'Habitation, logement (qu\'il s\'agisse ou non d\'un bâtiment entier).',
        "Travail, place (d'un domestique).",
        "Bâtiment, édifice destiné à un usage spécial.",
        "Entreprise commerciale.L'établissement où l'on travaille.",
        "Famille.",
        "Ensemble des personnes employées au service des grands personnages.",
        "Descendance, lignée des familles nobles.",
        "Qui a été fait à la maison, sur place.",
        "Particulièrement réussi, soigné.",
        "Particulier à une entreprise.",
        ],
        examples: ['La façade, les murs, le toit d\'une maison.', 'Les clés de la maison.',
        "Elle a fait de nombreuses maisons.",
        "Maison centrale, de correction, d'arrêt.",
        "Maison de détail, de gros.Les traditions de la maison.",
        "Une maison princière.",
        "La maison du roi.",
        "La maison d'Autriche.",
        "Tarte maison.",
        "Une engueulade maison.",
        "L'esprit maison.",
        ],
        synonyms: [	'demeure', 'domicile', 'foyer', 'logis', 'appartement', 'établissement', 'firme'],
        type: 'nom féminin',
        text: 'maison',
        lang: 'fr'
        })
        done()
      })
    })

    it('Should give back a bison form SPEED UP in English', (done) => {
      const google = new GoogleProvider(service, 'en', 'speed up', { fakeScraping: true })
      google.getData().then((res) => {
        expect(res).toMatchObject({
          definitions: ['an increase in speed, especially in a person\'s or machine\'s rate of working.'
          ],
          examples: [],
          synonyms: [],
          type: 'noun',
          text: 'speed up',
          lang: 'en'
        })
        done()
      })
    })

    it('Should not give translation in french for COUCOU in ENglish', (done) => {
      const google = new GoogleProvider(service, 'en', 'coucou', { fakeScraping: true })
      google.getData().then((res) => {
        expect(res).toMatchObject({
          definitions: [],
          examples: [],
          synonyms: [],
          type: ''
        })
        done()
      })
    })


    // ----- REAL WORLD SCRAPING -- USE GENTLY ---- //
    // it('Should scrap for real with scraper api', (done) => {
    //   const google = new GoogleProvider(service, 'en', 'television')
    //   google.getData().then((res) => {
    //     expect(res).toMatchObject({
    //       definitions: ['a system for converting visual images (with sound) into electrical signals, transmitting them by radio or other means, and displaying them electronically on a screen.",
    // +     "a device with a screen for receiving television signals.'],
    //       examples: ["\"the days before television\"",  "\"a colour television\""],
    //       synonyms: ["TV", "television set"],
    //       type: 'noun'
    //     })
    //     done()
    //   })
    // })
  })
})