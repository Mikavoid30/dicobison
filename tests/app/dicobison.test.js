
const { DicoBison } = require('../../dist/index.js');

describe('scrapData', function() {
  // When inputs are wrong should throw an error
  describe('inputs validation', () => {
    it('should throw an error when not array', () => {
      expect(() => new DicoBison({ fakeScraping: true }).scrapData('string')).toThrow()
    })
    it('should throw an error when wrong array', () => {
      expect(() => new DicoBison({ fakeScraping: true }).scrapData(['', ''])).toThrow()
    })
    it('should throw an error when array items are not well formatted', () => {
      expect(() => new DicoBison({ fakeScraping: true }).scrapData([{ lang: 'FR', texXxt: '' }])).toThrow()
    })
    it('should throw an error when array items are empty', () => {
      expect(() => new DicoBison({ fakeScraping: true }).scrapData([{ lang: 'FR', text: '' }])).toThrow()
    })
    it('should not throw an error when array is valid', () => {
      expect(() => new DicoBison({ fakeScraping: true }).scrapData([{ lang: 'FR', text: 'd' }])).not.toThrow()
    })
  })

  describe('when valid input', () => {
    it('should give us a well formatted response', async (done) => {
      const db = new DicoBison({
        debug: {fakeScraping: true}
      })
      const response = await db.scrapData([{ lang: 'fr', text: 'maison' }])
      response.forEach(item => {
        expect(typeof item.lang).toBe('string')
        expect(typeof item.text).toBe('string')
        expect(typeof item.type).toBe('string')
        expect(item.definitions).toBeInstanceOf(Array)
        expect(item.examples).toBeInstanceOf(Array)
        expect(item.synonyms).toBeInstanceOf(Array)
        done()
      })
    })

    it('should give us a well valid data', async (done) => {
      const db = new DicoBison({ fakeScraping: true })
      const response = await db.scrapData([{ lang: 'fr', text: 'maison' }, { lang: 'en', text: 'house' }])
      response.forEach(item => {
        expect(typeof item.lang).toBe('string')
        expect(typeof item.text).toBe('string')
        expect(typeof item.type).toBe('string')
        expect(item.definitions).toBeInstanceOf(Array)
        expect(item.examples).toBeInstanceOf(Array)
        expect(item.synonyms).toBeInstanceOf(Array)
      })
      expect(response[0]).toMatchObject({
        definitions: ['Bâtiment d\'habitation (immeuble, logement, résidence) ; spécialement bâtiment conçu pour un seul ou un petit nombre de foyers (pavillon, villa).',
        'Habitation, logement (qu\'il s\'agisse ou non d\'un bâtiment entier).',
        "Travail, place (d'un domestique).",
        "Bâtiment, édifice destiné à un usage spécial.",
        "Entreprise commerciale.L'établissement où l'on travaille.",
        "Famille.",
        "Ensemble des personnes employées au service des grands personnages.",
        "Descendance, lignée des familles nobles.",
        "Qui a été fait à la maison, sur place.",
        "Particulièrement réussi, soigné.",
        "Particulier à une entreprise.",
        ],
        examples: ['La façade, les murs, le toit d\'une maison.', 'Les clés de la maison.',
        "Elle a fait de nombreuses maisons.",
        "Maison centrale, de correction, d'arrêt.",
        "Maison de détail, de gros.Les traditions de la maison.",
        "Une maison princière.",
        "La maison du roi.",
        "La maison d'Autriche.",
        "Tarte maison.",
        "Une engueulade maison.",
        "L'esprit maison.",
        ],
        synonyms: [	'demeure', 'domicile', 'foyer', 'logis', 'appartement', 'établissement', 'firme'],
        type: 'nom féminin',
        text: 'maison',
        lang: 'fr'
      })
       expect(response[1]).toMatchObject({
        definitions: ['a building for human habitation, especially one that consists of a ground floor and one or more upper storeys.the people living in a house; a household.a noble, royal, or wealthy family or lineage; a dynasty.a dwelling that is one of several in a building.a building in which animals live or in which things are kept.',
         'a building in which people meet for a particular activity.a firm or institution.the Stock Exchange.a restaurant or inn.a brothel.a theatre.a performance in a theatre or cinema.',
         'a religious community that occupies a particular building.a residential building for pupils at a boarding school.each of a number of groups into which pupils at a day school are divided for games or competition.a college of a university.',
         'a legislative or deliberative assembly.(in the UK) the House of Commons or Lords; (in the US) the House of Representatives.used in formal debates that mimic the procedures of a legislative assembly.',
         'a style of electronic dance music typically having sparse, repetitive vocals and a fast beat.',
         'a twelfth division of the celestial sphere, based on the positions of the ascendant and midheaven at a given time and place, and determined by any of a number of methods.a twelfth division of the celestial sphere represented as a sector on an astrological chart, used in allocating elements of character and circumstance to different spheres of human life.',
         'old-fashioned term for bingo.used by a bingo player to announce that they have won.',
         '(of an animal or plant) kept in, frequenting, or infesting buildings.',
         'relating to a firm, institution, or society.(of a band or group) resident or regularly performing in a club or other venue.',
         'provide with shelter or accommodation.',
         'provide space for; contain or accommodate.fix (something) in a socket or mortise.'],
        examples: ['"a house of Cotswold stone""make yourself scarce before you wake the whole house""the power and prestige of the House of Stewart""a hen house"',
         '"a house of prayer""a publishing house""help yourself to a drink, compliments of the house!""a hundred musicians performed in front of a full house""tickets for the first house"',
         '"the Cistercian house at Clairvaux""a house of 45 boarders"',
         '"the sixty-member National Council, the country\'s upper house""a debate on the motion ‘This house would legalize cannabis’"',
         '"DJs specializing in techno, garage, and house"',
         '"a house journal""the house band"',
         '"they converted a disused cinema to house twelve employees"',
         '"the museum houses a collection of Roman sculpture"'],
        synonyms: [	'home',
         'place of residence',
         'homestead',
         'lodging place',
         'a roof over one\'s head; household',
         'family',
         'family circle',
         'ménage',
         'clan',
         'tribe; clan',
         'kindred',
         'family',
         'tribe',
         'race',
         'strain; ',
         'firm',
         'business',
         'company',
         'corporation',
         'enterprise',
         'establishment',
         'institution',
         'concern',
         'organization',
         'operation; inn',
         'bar',
         'tavern',
         'hostelry',
         'taproom; audience',
         'crowd',
         'those present',
         'listeners',
         'spectators',
         'viewers',
         'gathering',
         'assembly',
         'assemblage',
         'congregation; ',
         'legislative assembly',
         'legislative body',
         'chamber',
         'council',
         'parliament',
         'diet',
         'congress',
         'senate ',
         'accommodate',
         'provide accommodation for',
         'provide with accommodation',
         'give accommodation to',
         'make space for',
         'make room for',
         'give someone a roof over their head',
         'provide a roof over someone\'s head',
         'provide with a place to work',
         'harbour; ',
         'contain',
         'hold',
         'store',
         'cover; '],
        type: 'noun',
        text: 'house',
        printedWord: 'house',
        lang: 'en'
      })
      done()
    })
  })
});