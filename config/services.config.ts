import { Service } from '../types/service.type'

const services: Service[] = [
  {
    name: 'lexicala',
    baseUrl: 'https://dictapi.lexicala.com',
    paramsTemplate: '',
    langs: [
      'ar',
      'zh',
      'tw',
      'cs',
      'dk',
      'nl',
      'en',
      'fr',
      'de',
      'el',
      'he',
      'hi',
      'it',
      'ja',
      'ko',
      'la',
      'no',
      'pl',
      'pt',
      'br',
      'ru',
      'es',
      'sv',
      'th',
      'tr'
    ]
  }
]
export default services
