import services from './services.config'

export default function availableLanguages(): string[] {
  const array: string[] = []
  services.forEach((x) => array.push(...x.langs))
  return [...new Set(array)]
}
