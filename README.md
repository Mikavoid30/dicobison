# DicoBison

DicoBison will scrape multiple languages services to serve one purpose.
Get definitions, synonyms and examples from an expression in multipe languages. 

For example
  - **Expression:** "Maison"
  - **Languages:** { lang:"FR", text: "Maison" }, { lang: 'EN', text: 'House' }


The expected result has only one format:
```javascript
[
  {
    lang: 'FR',
    text: 'maison',
    printedWord: 'Maison',
    definitions: [/****/],
    examples: [/***/], 
    synonyms: [/****/]
  },
  {
    lang: 'EN',
    text: 'house',
    printedWord: 'House',
    definitions: [/****/],
    examples: [/***/], 
    synonyms: [/****/]
  },
  {
    lang: 'ES',
    text: 'casa',
    printedWord: 'Casa',
    definitions: [/****/],
    examples: [/***/], 
    synonyms: [/****/]
  },
  {
    lang: 'DE',
    text: 'haus',
    printedWord: 'Haus',
    definitions: [/****/],
    examples: [/***/], 
    synonyms: [/****/]
  }
]
```

# Usage

```javascript
import DicoBison from 'dicobison';

const db = new DicoBison({
  fakeScraping: false // will scrap real services
})

const responses = await db.scrapData([
  { lang: 'fr', text: 'maison' },
  { lang: 'en', text: 'house' },
])

```