export type Expression = {
  [key: string]: string
  lang: string
  text: string
}
