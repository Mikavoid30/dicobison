export type Service = {
  name: string
  langs: string[]
  baseUrl: string
  paramsTemplate: string
  test?: {
    inError: boolean
  }
}
