export type Bison = {
  originalExpression: any
  lang: string
  text: string
  type: string
  definitions: string[]
  examples: string[]
  synonyms: string[]
  printedWord?: string
}
