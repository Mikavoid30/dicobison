import { Expression } from '../types/expression.type'
const mandatoryProps = ['lang']

export function validateExpressions(expressions: Expression[]) {
  if (!expressions || !expressions.length)
    throw new Error('Expressions are invalid')
  for (let index in expressions) {
    if (!mandatoryProps.every((x: string) => !!expressions[index][x])) {
      throw new Error('Every Expressions must have text and lang props')
    }
  }

  return expressions.map((exp) => {
    return {
      ...exp,
      type: '',
      definitions: [],
      examples: [],
      synonyms: []
    }
  })
}
