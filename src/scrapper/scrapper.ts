import { IScrapper } from './scrapper.interface'
import defaultServices from '../../config/services.config'
import { Providers } from './providers/providers'
import { ConfigObject } from '../../types/configObject.type'
import { Service } from '../../types/service.type'

export class Scrapper implements IScrapper {
  lang: string
  text: string
  services: Service[]
  originalExpression: any

  constructor(
    lang: string,
    text: string,
    services: Service[],
    originalExpression: any
  ) {
    this.text = text.toLowerCase()
    this.lang = lang.toLowerCase()
    this.services = services || defaultServices
    this.originalExpression = originalExpression
  }

  async scrap(conf: ConfigObject = {}) {
    const validServices: Service[] = []
    const providers: any = []
    const defaultResponse = {
      originalExpression: this.originalExpression,
      definitions: [],
      examples: [],
      synonyms: [],
      type: '',
      text: '',
      lang: ''
    }
    try {
      // Get compatibles scrapping services
      this.services.forEach((service: Service) => {
        if (service.langs.indexOf(this.lang) >= 0) {
          validServices.push(service)
          providers.push(
            new Providers(
              service,
              this.lang,
              this.text,
              conf,
              this.originalExpression
            )
          )
        }
      })

      // try the first service
      const length = providers.length
      let response: any = { ...defaultResponse }
      for (let i = 0; i < length; i++) {
        try {
          const data = await providers[i].getData()
          response = { ...response, ...data }
          break
        } catch (e) {
          response = { ...defaultResponse }
        }
      }
      return response || defaultResponse
    } catch (e) {
      return defaultResponse
    }
  }
}
