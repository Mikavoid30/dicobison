import { DefaultProvider } from './_default.provider'
import request from 'request-promise'
import cheerio from 'cheerio'
import UserAgent from 'user-agents'
import fs from 'fs'
import path from 'path'
import { ConfigObject } from '../../../types/configObject.type'
import { Service } from '../../../types/service.type'
import { Bison } from '../../../types/bison.type'

export class GoogleProvider extends DefaultProvider {
  constructor(
    service: Service,
    lang: string,
    text: string,
    conf: ConfigObject,
    originalExpression: any
  ) {
    super(service, lang, text, conf, originalExpression)
    this.conf = conf
  }

  readHTML(fileName: string): Promise<any> {
    const stubPath =
      process.env.NODE_ENV === 'test'
        ? '../../../stubs/html/'
        : '../../../dist/stubs/html/'
    return new Promise(function(resolve, reject) {
      fs.readFile(path.join(__dirname, stubPath + fileName), (err, data) => {
        err ? reject(err) : resolve(data)
      })
    })
  }
  async tryRequest() {
    let definitions = []
    let examples = []
    let synonyms = []
    let type = ''
    try {
      let html = ''
      if (this.conf && this.conf.fakeScraping) {
        try {
          html = await this.readHTML(
            `${this.service.name}-${this.lang}-${this.text}.html`
          )
        } catch (e) {
          html = await this.readHTML(`test.html`)
        }
      } else {
        html = await request({
          uri: this.url || '',
          headers: {
            'Accept-Language':
              'en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3',
            'User-Agent': new UserAgent().toString(), // RandomUserAgent,
            referer: `https://www.google.${
              this.lang === 'en' ? 'com' : this.lang
            }`
          }
        })
      }

      const $ = cheerio.load(html)
      const word = $('div.dDoNo span')
        .first()
        .text()
      if (!word || !word.length) throw new Error('word does not exist')
      this.printedWord = word

      const printedLanguage = $('.lr_dct_ent.vmod.XpoqFe > div')
        .first()
        .data('language-code')

      const splitted = printedLanguage && printedLanguage.split('-')
      if (splitted && splitted.length && splitted[0] !== this.lang)
        throw new Error('Result have unexpected language')

      const main = $('.lr_dct_ent.vmod.XpoqFe')
        .first()
        .find('.lr_dct_sf_h')

      type = main
        .first()
        .find('i > span')
        .text()
        .trim()

      definitions = $('.lr_dct_sf_sens')
        .find('.lr_dct_sf_sen')
        .map((i, el: CheerioElement) => {
          return $(el)
            .find('div[data-dobid="dfn"]')
            .text()
            .trim()
        })
        .get()
      examples = $('.lr_dct_sf_sens')
        .find('.lr_dct_sf_sen')
        .map((i, el: CheerioElement) => {
          return $(el)
            .find('div[data-dobid="dfn"]')
            .next('.vmod')
            .find('.vk_gy')
            .text()
            .trim()
        })
        .get()
        .filter((x: any) => x.length)
      synonyms = $('.lr_dct_sf_sens')
        .find('.lr_dct_sf_sen')
        .map((i, el: CheerioElement) => {
          return $(el)
            .find('div[data-dobid="dfn"]')
            .next('.vmod')
            .next('.vmod')
            .find(
              'td.lr_dct_nyms_ttl + td > span:not([data-log-string="synonyms-more-click"])'
            )
            .text()
            .split(', ')
        })
        .get()
        .filter((x: any) => x.length)
    } catch (e) {
      return Promise.resolve({
        originalExpression: {},
        definitions: [],
        examples: [],
        synonyms: [],
        type: '',
        lang: '',
        text: '',
        printedWord: ''
      })
    }

    return Promise.resolve({
      originalExpression: this.originalExpression,
      definitions,
      examples,
      synonyms,
      type,
      text: this.text,
      lang: this.lang,
      printedWord: this.printedWord
    })
  }

  getData(): Promise<Bison> {
    return new Promise(async (resolve, reject) => {
      try {
        const {
          originalExpression = {},
          definitions = [],
          examples = [],
          synonyms = [],
          type = '',
          lang = '',
          text = '',
          printedWord = ''
        } = await this.tryRequest()
        return resolve({
          originalExpression,
          lang,
          definitions,
          examples,
          synonyms,
          type,
          text,
          printedWord
        })
      } catch (e) {
        return Promise.resolve({
          originalExpression: {},
          definitions: [],
          examples: [],
          synonyms: [],
          type: e,
          lang: '',
          text: '',
          printedWord: ''
        })
      }
    })
  }
}
