import { IProvider } from './providers'
import { config } from 'dotenv'
import { ConfigObject } from '../../../types/configObject.type'
import { Service } from '../../../types/service.type'
import { Bison } from '../../../types/bison.type'

export class DefaultProvider implements IProvider {
  service: Service
  inError: boolean // Test purpose
  url: string
  lang: string
  text: string
  printedWord?: string
  conf?: ConfigObject
  originalExpression: any

  constructor(
    service: Service,
    lang: string,
    text: string,
    conf: ConfigObject,
    originalExpression: any
  ) {
    config()
    this.conf = conf
    this.service = service
    this.inError = !!service.test && service.test.inError
    this.lang = lang
    this.text = text
    this.originalExpression = originalExpression
    if (!this.service.langs.includes(this.lang))
      throw new Error('language not supported')

    // Build the url
    this.url = encodeURI(
      `${this.service.baseUrl}/${this.fillTemplate(
        this.service.paramsTemplate
      )}`
    )
  }

  fillTemplate(str: string) {
    let string = str.replace('{{lang}}', this.lang)
    string = string.replace('{{text}}', `"${this.text.replace(' ', '+')}"`)
    return string
  }

  getData(): Promise<Bison> {
    return new Promise((resolve, reject) => {
      return resolve({
        originalExpression: {},
        definitions: [],
        examples: [],
        synonyms: [],
        type: '',
        text: '',
        lang: '',
        printedWord: ''
      })
    })
  }
}
