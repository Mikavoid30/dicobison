import { GoogleProvider } from './google.provider'
import { ConfigObject } from '../../../types/configObject.type'
import { Bison } from '../../../types/bison.type'
import { Service } from '../../../types/service.type'
import { LexicalaProvider } from './lexicala.provider'

export interface IProvider {
  service: Service
  getData(): Promise<Bison> | Bison | null
}

export class Providers {
  classes: any = {
    google: GoogleProvider,
    lexicala: LexicalaProvider
  }

  constructor(
    service: Service,
    lang: string,
    text: string,
    conf: ConfigObject = {},
    originalExpression: any
  ) {
    let dynClass: any = this.classes[service.name]
    return new dynClass(service, lang, text, conf, originalExpression)
  }
}
