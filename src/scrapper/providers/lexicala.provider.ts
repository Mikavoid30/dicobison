import { DefaultProvider } from './_default.provider'
import { Service } from '../../../types/service.type'
import { ConfigObject } from '../../../types/configObject.type'
import { Bison } from '../../../types/bison.type'
import axios, { AxiosInstance } from 'axios'

type LexicalaSense = {
  id: string
  definition: string
}

type LexicalaSearchResults = {
  id: string
  language: string
  headword: {
    text: string
    pos: string
  }
  senses: LexicalaSense[]
}

type LexicalaAPIResponseSensesData = {
  id: string
  definition: string
  subcategorization: string
  translations: any
  examples: Array<{
    text: string
    translations: any
  }>
  see_also?: string
}

type LexicalaAPIResponseData = {
  n_results: number
  page_number: number
  results_per_page: number
  n_pages: number
  available_n_pages: number
  results: LexicalaSearchResults[]
}

export class LexicalaProvider extends DefaultProvider {
  fetcher: AxiosInstance
  originalExpression: any
  constructor(
    service: Service,
    lang: string,
    text: string,
    conf: ConfigObject,
    originalExpression: any
  ) {
    super(service, lang, text, conf, originalExpression)
    this.conf = conf
    this.originalExpression = originalExpression
    this.fetcher = axios.create({
      baseURL:
        service.baseUrl ||
        process.env.LEXICALA_URL ||
        'https://dictapi.lexicala.com',
      auth: {
        username: process.env.LEXICALA_USERNAME || '',
        password: process.env.LEXICALA_PASSWORD || ''
      }
    })
  }

  async tryRequests(): Promise<Bison> {
    if (this.conf && this.conf.fakeScraping) {
      const sleep = () => new Promise((resolve) => setTimeout(resolve, 500))
      await sleep()
      return this.fakeAnswer()
    }
    const responseObject: Bison = {
      originalExpression: this.originalExpression,
      definitions: [],
      examples: [],
      synonyms: [],
      type: '',
      lang: this.lang || '',
      text: this.text || '',
      printedWord: ''
    }
    try {
      // Search
      const searchData = (await this.fetcher.get(
        `search?source=global&language=${this.lang.toLowerCase()}&morph=false&text=${this.text.toLowerCase()}`
      )) as any

      const { data }: { data: LexicalaAPIResponseData } = searchData
      if (!data || !data.results || !data.results.length) return responseObject

      for (let i = data.n_results - 1; i >= 0; i--) {
        /**
         * @todo incude text and pos (noun, verb...)
         */
        const result = data.results[i]
        const senses =
          result && result.senses && result.senses.length
            ? result.senses.filter(
                (sense: LexicalaSense) =>
                  sense.id && sense.definition && sense.definition.length
              )
            : []
        const sensesLen = senses.length

        for (let i = 0; i < sensesLen; i++) {
          responseObject.definitions.push(senses[i].definition)
          const senseDetails = await this.fetchSenseDetails(senses[i].id)
          responseObject.examples = [
            ...responseObject.examples,
            ...senseDetails.examples
          ]
          responseObject.synonyms = [
            ...responseObject.synonyms,
            ...senseDetails.synonyms
          ]
        }
      }
    } catch (e) {
      console.error(e)
      return responseObject
    }

    return responseObject
  }

  fakeAnswer() {
    return {
      originalExpression: this.originalExpression,
      definitions: [this.text + ' one fake def - ' + this.lang, 'another one'],
      examples: [this.text + ' One fake example - ' + this.lang, 'another one'],
      synonyms: [this.text, 'synonyms', 'fake', this.lang],
      type: '',
      lang: this.lang || '',
      text: this.text || '',
      printedWord: ''
    }
  }

  async fetchSenseDetails(senseId: string) {
    try {
      const {
        data
      }: { data: LexicalaAPIResponseSensesData } = await this.fetcher(
        `/senses/${senseId}`
      )
      if (!data) throw new Error('No data for examples and synonyms')

      const examples = data.examples.map(
        (example: { text: string }) => example.text
      )
      const synonyms = data.see_also ? data.see_also.split(', ') : []

      return {
        examples,
        synonyms
      }
    } catch (e) {
      return Promise.resolve({
        examples: [],
        synonyms: []
      })
    }
  }

  getData(): Promise<Bison> {
    return new Promise(async (resolve, reject) => {
      try {
        const bison = await this.tryRequests()
        return resolve(bison)
      } catch (e) {
        return Promise.resolve({
          definitions: [],
          examples: [],
          synonyms: [],
          type: e,
          lang: '',
          text: '',
          printedWord: ''
        })
      }
    })
  }
}
