import { ConfigObject } from '../../types/configObject.type'
import { Bison } from '../../types/bison.type'

export interface IScrapper {
  scrap(conf?: ConfigObject): Promise<Bison[]>
}
