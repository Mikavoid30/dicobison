import { config } from 'dotenv'
import { validateExpressions } from './src/validator'
import { ConfigObject } from './types/configObject.type'
import { Scrapper } from './src/scrapper/scrapper'
import services from './config/services.config'
import { Expression } from './types/expression.type'

export class DicoBison {
  conf: any

  constructor(configObject: ConfigObject = {}) {
    config() // .env
    this.conf = configObject
  }

  scrapData(expressions: Expression[]) {
    validateExpressions(expressions)
    return new Promise(async (resolve, reject) => {
      let proms: any[] = []
      expressions.forEach(async (expression) => {
        const scrapper = new Scrapper(
          expression.lang,
          expression.text,
          services,
          expression
        )
        proms.push(scrapper.scrap(this.conf))
      })
      return resolve(await Promise.all(proms))
    })
  }
}
